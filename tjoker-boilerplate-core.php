<?php
/**
 * Plugin Name:       TJoker Boilerplate Core
 * Plugin URI:        http://themejoker.com/
 * Description:       Required plugin for TJoker Theme.
 * Version:           2.0.0
 * Author:            Saiful Islam Ananda
 * Author URI:        http://saifulananda.me/
 * License:           GNU General Public License v2 or later
 * Text Domain:       tjoker-boilerplate-core 
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Function that runs during plugin activation.
 */
function tjoker_core_activate() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tjoker-core-activator.php';
	TJoker_Core_Activator::activate();
}

/**
 * Function that runs during plugin deactivation.
 */
function tjoker_core_deactivate() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tjoker-core-deactivator.php';
	TJoker_Core_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'tjoker_core_activate' );
register_deactivation_hook( __FILE__, 'tjoker_core_deactivate' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-tjoker-core.php';

/**
 * Begins execution of the plugin.
 *
 * @since    1.0.0
 */
function run_TJoker_Core() {

	$plugin = new TJoker_Core();
	$plugin->run();

}
run_TJoker_Core();
