<?php

if( ! function_exists( 'tj_print' ) ) :
	function tj_print( $data ) {
		//if( ! WP_DEBUG ) return;
		echo '<pre>';
		if( is_array( $data ) || is_object( $data ) ) {
			print_r( $data );
		}
		else {
			echo $data;
		}
		echo '</pre>';
	}
	endif;



/***************************************************/
		/*TJoker Get Theme Option function*/
/***************************************************/
if ( !function_exists('tj_opt') ) :
	function tj_opt($key, $default = ''){
		global $tj_theme_opt;
		
		if ( isset($tj_theme_opt[$key]) && !empty($tj_theme_opt[$key]) ) {		
			$option = stripslashes_deep($tj_theme_opt[$key]);
		}
		else {
			$option = $default;
		}
		
		return $option;
	}
endif;

if ( !function_exists('tj_opts') ) :
	function tj_opts($id, $fallback = false, $key = false, $default = '')
	{
		global $tj_theme_opt;
		
		if ( $fallback == false ) {
			$fallback = '';
		}	
		$option = ( isset( $tj_theme_opt[$id] ) && $tj_theme_opt[$id] !== '' ) ? $tj_theme_opt[$id] : $fallback;
		if ( !empty( $tj_theme_opt[$id] ) && $key ) {
			$option = $tj_theme_opt[$id][$key];
		}	
		if ( empty($option) ) {
			$option = $default;
		}

		return $option;
	}
endif;
/***************************************************/
   /*TJoker Return Array or String Data function*/
/***************************************************/
if ( !function_exists('tj_get_socials') ) :
	function tj_get_socials()
	{
		$icons = array(
			'facebook'	=> array('facebook',	'Facebook'),
			'google'	=> array('google-plus',	'Google+'),
			'twitter'	=> array('twitter',		'Twitter'),
			'linkedin'	=> array('linkedin',	'LinkedIn'),
			'dribbble'	=> array('dribbble',	'Dribbble'),
			'instagram'	=> array('instagram',	'Instagram'),
			'pinterest'	=> array('pinterest',	'Pinterest'),
			'foursquare'=> array('foursquare',	'Foursquare'),
			'tumblr'	=> array('tumblr',		'Tumblr'),
			'youtube'	=> array('youtube',		'Youtube'),
			'vimeo'		=> array('vimeo-square','Vimeo'),
			'flickr'	=> array('flickr',		'Flickr'),
			);
		return $icons;
	}
endif;

if ( !function_exists('tj_get_kc_tag_options') ) :
	function tj_get_kc_tag_options(){
		$tjstatus = get_tags(array('hide_empty' => false));
		$TJoker = array(
				''	=> __( 'Select', 'theme-joker-core' ),
			);
		foreach ($tjstatus as $status) {
			$TJoker[$status->term_id] = __( ucfirst($status->name), 'theme-joker-core' );
		}
		return $TJoker;
	}
endif;



if ( !function_exists('tj_get_kc_tax_options') ) :
	function tj_get_kc_tax_options($taxonomy){
		$tjtax = get_terms($taxonomy, array('hide_empty' => false));
		$TJoker = array(
				''	=> __( 'Select', 'theme-joker-core' ),
			);
		foreach ($tjtax as $tax) {
			$TJoker[$tax->term_id] = __( ucfirst($tax->name), 'theme-joker-core' );
		}
		return $TJoker;
	}
endif;


/***************************************************/
 /*TJoker Return Array or String Data function End*/
/***************************************************/




/***************************************************/
	   /*TJoker Thumbnail Images Data Function*/
/***************************************************/
if ( !function_exists('tj_thumbnail') ) {
	function tj_thumbnail($size = 'full')
	{
		if ( has_post_thumbnail() ) {
			$out = get_the_post_thumbnail(get_the_ID(), $size);
		} else {
			$out = '<img src="'. get_template_directory_uri() .'/assets/images/default-image.png" alt="" />';
		}
		
		return $out;
	}
}


/***************************************************/
		/*TJoker Post Format Data function*/
/***************************************************/
if ( ! function_exists( 'tj_get_post_format_data' ) ) :
	function tj_get_post_format_data($TJid = 0) {
		$tj_slide_num = array('one', 'two', 'three'); 
        $tj_blog_array = array(); 

        $TJformat = get_post_format($TJid);
        
        if(get_post_format($TJid) =='' || get_post_meta( $TJid, 'tj_feature_image_show', true) == 'Y'):
            $TJformat = 'image';
        endif;
		$XtrimIT ='';
		switch (trim($TJformat)) {
			case 'video':
				$video_type = get_post_meta( $TJid, 'tj_post_video_type', true);
				if($video_type === 'Y') :
					$youtube_video_id = get_post_meta( $TJid, 'tj_youtube_video_id', true);
					if(!empty($youtube_video_id)):
						$XtrimIT .= '<div class="single-image">';
						    $XtrimIT .= '<iframe src="https://www.youtube.com/embed/'.$youtube_video_id.'" allowfullscreen></iframe>';
						$XtrimIT .= '</div>';
					endif;
				endif;

				if($video_type === 'V') :
					$vimeo_video_id = get_post_meta( $TJid, 'tj_vimeo_video_id', true);
					if(!empty($vimeo_video_id)):
						$XtrimIT .= '<div class="single-image">';
						    $XtrimIT .= '<iframe src="https://player.vimeo.com/video/'.$vimeo_video_id.'" allowfullscreen></iframe>';
						$XtrimIT .= '</div>';
					endif;
				endif;

				break;
			case 'audio':
				$audio_track_id = get_post_meta( $TJid, 'tj_audio_track_id', true);
				if(!empty($audio_track_id)):
					$XtrimIT .= '<div class="single-image embed-responsive embed-responsive-16by9">';
					    $XtrimIT .= '<iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/'.absint($audio_track_id).'&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>';
					$XtrimIT .= '</div>';
				endif;				
				break;
			case 'image':
				$post_fimage = wp_get_attachment_url(get_post_thumbnail_id($TJid));
				$psot_title = get_the_title($TJid);
				if(!empty($post_fimage)):
					$XtrimIT .= '<div class="single-image">';
						$XtrimIT .= '<img class="img-responsive" src="'.esc_url($post_fimage).'" alt="'. esc_attr($psot_title) .'" title="'. esc_attr($psot_title) .'">';
					$XtrimIT .= '</div>';
				endif;
				break;
			case 'gallery':
				$gallery_images = array();
		        foreach ($tj_slide_num as $num):
		            $link = get_post_meta( $TJid, 'tj_gallery_img_'.$num, true);
		            if(!empty($link) && isset($link)):
		                $gallery_images[] = $link;
		            endif;
		        endforeach;
				if(!empty($gallery_images)):
					$XtrimIT .= '<div class="carousel slide carousel-fade single-image" data-ride="carousel" id="blog-post-slider">';
					    //$XtrimIT .= '<a class="post-thumbnail" data-animsition-out="fade-out-up-sm" data-animsition-out-duration="500" href="'.esc_url($value['guid']).'">';
							$XtrimIT .= '<div class="carousel-inner" role="listbox">';
							$active = 'active';
							foreach ($gallery_images as $value):
								$XtrimIT .= '<div class="item '.$active.'"><img alt="blog" src="'.wp_get_attachment_url($value).'"></div>';
								$active = '';
							endforeach;
							$XtrimIT .= '</div>';
						//$XtrimIT .= '</a> ';
						$XtrimIT .= '<a class="left slide-control" data-slide="prev" href="#blog-post-slider" role="button">';
							$XtrimIT .= '<i class="fa fa-angle-left"></i>';
							//$XtrimIT .= '<span class="sr-only">Previous</span>';
						$XtrimIT .= '</a> ';
						$XtrimIT .= '<a class="right slide-control" data-slide="next" href="#blog-post-slider" role="button">';
							$XtrimIT .= '<i class="fa fa-angle-right"></i> ';
							//$XtrimIT .= '<span class="sr-only">Next</span>';
						$XtrimIT .= '</a>';
					$XtrimIT .= '</div>';
				endif;
				break;
			default: 
				# code...
				break;
		}
		return $XtrimIT;
	}
endif;





/***************************************************/
		/*TJoker Navigation Data function*/
/***************************************************/
if ( ! function_exists( 'tj_paginate_nav' ) ) :
	function tj_paginate_nav() {
		// Don't print empty markup if there's only one page.
		if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
			return;
		}

		$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
		$pagenum_link = html_entity_decode( get_pagenum_link() );
		$query_args   = array();
		$url_parts    = explode( '?', $pagenum_link );

		if ( isset( $url_parts[1] ) ) {
			wp_parse_str( $url_parts[1], $query_args );
		}

		$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
		$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

		$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
		$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

		// Set up paginated links.
		$links = paginate_links( array(
			'base'     => $pagenum_link,
			'format'   => $format,
			'total'    => $GLOBALS['wp_query']->max_num_pages,
			'current'  => $paged,
			'add_args' => array_map( 'urlencode', $query_args ),
			'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
			'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
			'type'      => 'array',
		) );

		if ( $links ) :

		?>
		<div class="pagination-area">
			<ul class="">
				<?php foreach ( $links as $key => $page_link ) : ?>
					<li class="<?php if ( strpos( $page_link, 'current' ) !== false ) { echo ' active'; } ?>"><?php echo str_replace('span', 'a', $page_link)?></li>
				<?php endforeach ?>
			</ul>
		</div><!-- .navigation -->
		<?php
		endif;
	}
endif;


function getBanglaDate($date){
	$engArray = array(
	 1,2,3,4,5,6,7,8,9,0,
	 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',
	 'am', 'pm', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
	);
	 $bangArray = array(
	 '১','২','৩','৪','৫','৬','৭','৮','৯','০',
	 'জানুয়ারি', 'ফেব্রুয়ারি', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর',
	 'এ.এম', 'পি.এম', 'শনিবার', 'রবিবার', 'সোমবার', 'মঙ্গলবার', 'বুধবার', 'বৃহস্পতিবার', 'শুক্রবার',
	 );
	 
	 $converted = str_replace($engArray, $bangArray, $date);
	 return $converted;
}

/***************************************************/
		/*TJoker Social Link Data function*/
/***************************************************/
if ( ! function_exists( 'tj_social_link_data' ) ) :
	function tj_social_link_data(){
		global $tj_opt;
		$TJsocial = (array)tj_opts('tj_social_url');
//print_r($TJsocial);	
		$tjlink = '';
		$tjlink .= '<nav class="social-media-area">';
			$tjlink .= '<ul>';
			foreach ($TJsocial as $icon => $link):
				if( $link != '#') :
					$tjlink .= '<li><a title="'. ucwords($icon) .'" href="'.esc_url($link).'"><i class="fa fa-'. $icon .'" aria-hidden="true"></i></a></li>';
				endif;
			endforeach;
			$tjlink .= '</ul>';
		$tjlink .= '</nav>';
		
		return $tjlink;

	}
endif;

if ( ! function_exists( 'tj_footer_social_link_data' ) ) :
	function tj_footer_social_link_data(){
		global $tj_opt;
		$TJsocial = (array)tj_opts('tj_social_url');
		$tjlink = '';

			$tjlink .= '<ul class="unstyled footer-social">';
			foreach ($TJsocial as $icon => $link):
				if( $link != '#') :
					$tjlink .= '<li><a title="'. ucwords($icon) .'" href="'.esc_url($link).'"><span class="social-icon"><i class="fa fa-'. $icon .'" aria-hidden="true"></i></span></a></li>';
				endif;
			endforeach;
			$tjlink .= '</ul>';
		
		return $tjlink;

	}
endif;

if ( ! function_exists( 'tj_single_page_cat_link' ) ) :
	function tj_single_page_cat_link($TJid, $cat, $limit= 4){
	    $cat_trms = wp_get_object_terms($TJid, $cat);
	    $count = 0;
	    $cat_html = '';
	    foreach ($cat_trms as $trms) :
	    	++$count;
	        $cat_html .= '<a href="'.get_term_link($trms).'">'.ucwords(esc_attr($trms->name)).'</a> ';
	        if($count == $limit)
	        	break;
	    endforeach;
		
		return $cat_html;

	}
endif;
if ( ! function_exists( 'tj_post_cat_link' ) ) :
	function tj_post_cat_link($TJid, $cat, $limit= 4){
	    $cat_trms = wp_get_object_terms($TJid, $cat);
	    $count = 0;
	    $cat_html = '';
	    foreach ($cat_trms as $trms) :
	    	++$count;
	        $cat_html .= '<a class="cat-link" href="'.get_term_link($trms).'">'.ucwords(esc_attr($trms->name)).'</a> ';
	        if($count == $limit)
	        	break;
	    endforeach;
		
		return $cat_html;

	}
endif;
/***************************************************/
		/*TJoker Social Shear Data function*/
/***************************************************/
if ( ! function_exists( 'tj_social_shear_link_data' ) ) :
	function tj_social_shear_link_data($TJid){

		$TJoker = '';
        $TJoker .= '<ul class="share-link">';
            $TJoker .= '<li class="hvr-bounce-to-right"><a href="https://www.facebook.com/sharer/sharer.php?u='. esc_url(get_the_permalink($TJid)) .'" target="_blank"><i class="fa fa-facebook"></i> Facebook </a></li>';
            $TJoker .= '<li class="hvr-bounce-to-right"><a href="https://twitter.com/intent/tweet?source='. esc_url(get_the_permalink($TJid)).'&amp;text='. esc_textarea(get_post_meta($TJid, 'xtrimIT_project_short_des',true)) .'" target="_blank"><i class="fa fa-twitter-square"></i> Twitter </a></li>';
            $TJoker .= '<li class="hvr-bounce-to-right"><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url='. esc_url(get_the_permalink($TJid)) .'&amp;title='. get_the_title($TJid) .'&amp;summary='. esc_textarea(get_post_meta($TJid, 'xtrimIT_project_short_des',true)) .'" target="_blank"><i class="fa fa-linkedin"></i> Linkedin </a></li>';
            $TJoker .= '<li class="hvr-bounce-to-right"><a href="https://plus.google.com/share?url='. esc_url(get_the_permalink($TJid)) .'" target="_blank"><i class="fa fa-google-plus"></i> +Google </a></li>';
        $TJoker .= '</ul>';

		return $TJoker;
	}
endif; 