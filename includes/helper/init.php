<?php 
/**
 * 
 * Theme Include Init File.
 *
 * @since 1.0.0
 */

	// File Security Check
	if ( ! defined( 'ABSPATH' ) ): 
	    exit;
	endif;

	if (file_exists(plugin_dir_path(dirname(__FILE__)) . 'helper/common-function.php')):
	    require_once plugin_dir_path(dirname(__FILE__)) . 'helper/common-function.php';
	endif;

	if (file_exists(plugin_dir_path(dirname(__FILE__)) . 'helper/helper.php')):
	    require_once plugin_dir_path(dirname(__FILE__)) . 'helper/helper.php';
	endif;	