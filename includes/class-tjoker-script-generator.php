<?php
/**
 * The file that defines Script Generator
 *
 * @link       http://themejoker.com/
 * @since      1.0.0
 *
 * @package    TJoker_Core
 * @subpackage TJoker_Core/includes
 */

/**
 * The Dynamicly Secipt generator class.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    TJoker_Core
 * @subpackage TJoker_Core/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */
class TJ_Script_Generator{

    

    /**
     * @since    1.1.0
     */
    public function __construct(){


    }
    public function tj_generate_nivoSlider_script($sliderID){

      //$TJoker = '';
      return $TJoker = '
      <script type="text/javascript">
        (function($) {
            "use strict";
            jQuery(document).on(\'ready\', function(){
            $("#'.$sliderID.'").nivoSlider({
                effect: \'fade\',
                slices: 15,
                boxCols: 8,
                boxRows: 4,
                animSpeed: 500,
                pauseTime: 5000,
                startSlide: 0,
                directionNav: true,
                controlNavThumbs: false,
                pauseOnHover: false,
                manualAdvance: false
            });
            });
        })(jQuery);
      </script>';
    }

    public function tj_generate_owlCarousel_script($sliderID){

        return $TJoker = '
        <script type="text/javascript">
            (function($) {
                "use strict";
                jQuery(document).on(\'ready\', function(){
                    $("#'. $sliderID .'").owlCarousel({
                        items : 4,     
                        nav: true,
                        navText: ["<i class=\'fa fa-angle-left\'></i>", "<i class=\'fa fa-angle-right\'></i>"],
                        dots: false,
                        responsive:{
                            0:{
                              items:1
                            },
                            480:{
                              items:1, 
                              nav:false

                            },
                            678:{

                              items:2, 
                              center:false
                            },
                            768:{

                              items:2,
                              margin:20,
                              center:false 

                            },
                            1200:{
                              items:4,
                              loop:false,
                              margin: 0
                            }
                        } 
                    });
                });
            })(jQuery);
        </script>';
    }
}

