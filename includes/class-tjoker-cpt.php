<?php

/**
 * Custom Post Type Class
 * Used to help create custom post types taxnomy for Wordpress.
 * @author  Saiful Islam Ananda
 * @link    
 * @version 1.0.0
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */

class TJoker_CPT{

    /**
     * Post type name.
     *
     * @var string $post_type Holds the name of the post type.
     */
    private $post_type;

    /**
    * User submitted args assigned on __construct().
    *
    * @var array $args Holds the user submitted post type argument.
    */
    public $args;

    /**
     * Holds the singular name of the post type. This is a human friendly
     * name, capitalized with spaces assigned on __construct().
     *
     * @var string $singular Post type singular name.
     */
    private $singular;

    /**
     * Holds the plural name of the post type. This is a human friendly
     * name, capitalized with spaces assigned on __construct().
     *
     * @var string $plural Singular post type name.
     */
    private $plural;

    /**
     * Post type slug. This is a robot friendly name, all lowercase and uses
     * hyphens assigned on __construct().
     *
     * @var string $slug Holds the post type slug name.
     */
    private $slug;

    /**
     * Textdomain used for translation. Use the set_textdomain() method to set a custom textdomain.
     *
     * @var string $textdomain Used for internationalising. Defaults to "xtrimit" without quotes.
     */
    public $textdomain = 'theme-joker-core';



    /**
     * Sets default values, registers the passed post type, and
     * listens for when the post is saved.
     *
     * @param string $post_type The name of the desired post type.
     * @param string $singular Singular name for this post type.
     * @param string $plural Plural name of this post type.
     * @param array @args Override the options.
     */
    function __construct($post_type, $singular = "", $plural = "", $args = array()){
          
        $this->post_type    =   strtolower(str_replace(' ', '-', $post_type));
        if(!empty($singular)):
            $this->singular     =   $singular;
        else:
            $this->singular     =   $this->xtrimit_get_singular($post_type);
        endif;
        if(!empty($plural)):
            $this->plural     =   $plural;
        else:
            $this->plural     =   $this->xtrimit_get_plural($post_type);
        endif;
        
        $this->slug         =   $this->xtrimit_get_slug($this->post_type);
        
        $this->args         = (array)$args;
        // First step, register that new post type
        $this->xtrimit_init(array($this, "xtrimit_register_post_type"));

        $this->xtrimit_add_filter( 'enter_title_here', array($this, 'xtrimit_post_title'));
    }


    /**
     * @param string $action Name of the action.
     * @param string $function Function to hook that will run on action.
     * @param integet $priority Order in which to execute the function, relation to other functions hooked to this action.
     * @param integer $accepted_args The number of arguments the function accepts.
     */
    function xtrimit_add_action( $action, $function, $priority = 10, $accepted_args = 1 ) {
        // Pass variables into WordPress add_action function
        add_action( $action, $function, $priority, $accepted_args );
    }

    /**
     * @param  string  $action           Name of the action to hook to, e.g 'init'.
     * @param  string  $function         Function to hook that will run on @action.
     * @param  int     $priority         Order in which to execute the function, relation to other function hooked to this action.
     * @param  int     $accepted_args    The number of arguements the function accepts.
     */
    function xtrimit_add_filter( $action, $function, $priority = 10, $accepted_args = 1 ) {
        // Pass variables into Wordpress add_action function
        add_filter( $action, $function, $priority, $accepted_args );
    }

    /**
     * Helper method 
     * That passed a function to the 'init' WP action
     * @param function $cb Passed callback function.
     */
    function xtrimit_init($cb){
        add_action("init", $cb);
    }


    /**
     * Helper method
     * That passed a function to the 'admin_init' WP action
     * @param function $cb Passed callback function.
     */
    function xtrimit_admin_init($cb){
        add_action("admin_init", $cb);
    }


    /**
     * Get slug
     * 
     * Creates an url friendly slug.
     *
     * @param  string $slug Name to slugify.
     * @return string $slug Returns the slug.
     */
    function xtrimit_get_slug( $slug = null ) {

        // If no name set use the post type name.
        if ( ! isset( $slug ) ) {
            $slug = $this->post_type;
        }

        // Name to lower case.
        $slug = strtolower( $slug );

        // Replace spaces with hyphen.
        $slug = str_replace( " ", "-", $slug );

        // Replace underscore with hyphen.
        $slug = str_replace( "_", "-", $slug );

        return $slug;
    }


    /**
     * Get plural
     *
     * Returns the friendly plural name.
     *
     * @param  string $p_name The slug name you want to pluralize.
     * @return string the friendly pluralized name.
     */
    function xtrimit_get_plural( $p_name = null ) {
        // If no name is passed the post_type is used.
        if ( ! isset( $p_name ) ) {
            $p_name = $this->post_type;
        }
        // Return the plural name. Add 's' to the end.
        return $this->xtrimit_get_human_friendly( $p_name ) . 's';
    }


    /**
     * Get singular
     *
     * Returns the friendly singular name.
     *
     * @param string $s_name The slug name you want to human readable singular.
     * @return string The friendly singular name.
     */
    public function xtrimit_get_singular( $s_name = null ) {
        // If no name is passed the post_type is used.
        if ( ! isset( $s_name ) ) {
            $s_name = $this->post_type;
        }
        // Return the string.
        return $this->xtrimit_get_human_friendly( $s_name );
    }


    /**
     * Get human friendly
     *
     * Returns the human friendly name.
     *
     *    ucwords      capitalize words
     *    strtolower   makes string lowercase before capitalizing
     *    str_replace  replace all instances of hyphens and underscores to spaces
     *
     * @param string $name The name you want to make friendly.
     * @return string The human friendly name.
     */
    function xtrimit_get_human_friendly( $name = null ) {

        // If no name is passed the post_type is used.
        if ( ! isset( $name ) ) {
            $name = $this->post_type;
        }
        // Return human friendly name.
        return ucwords( strtolower( str_replace( "-", " ", str_replace( "_", " ", $name ) ) ) );
    }

    /**
     * Register Post Type
     *
     * @see http://codex.wordpress.org/Function_Reference/register_post_type
     */
    function xtrimit_register_post_type() {
        
        $post_type  =   $this->post_type;
        $plural     =   $this->plural;
        $singular   =   $this->singular;
        $slug       =   $this->slug;

        // Default labels.
        $labels = array(
            'name'               => sprintf( __( '%s', 'theme-joker-core' ), $plural ),
            'singular_name'      => sprintf( __( '%s', 'theme-joker-core' ), $singular ),
            'menu_name'          => sprintf( __( '%s', 'theme-joker-core' ), $plural ),
            'all_items'          => sprintf( __( '%s', 'theme-joker-core' ), $plural ),
            'add_new'            => __( 'Add New', 'theme-joker-core' ),
            'add_new_item'       => sprintf( __( 'Add New %s', 'theme-joker-core' ), $singular ),
            'edit_item'          => sprintf( __( 'Edit %s', 'theme-joker-core' ), $singular ),
            'new_item'           => sprintf( __( 'New %s', 'theme-joker-core' ), $singular ),
            'view_item'          => sprintf( __( 'View %s', 'theme-joker-core' ), $singular ),
            'search_items'       => sprintf( __( 'Search %s', 'theme-joker-core' ), $plural ),
            'not_found'          => sprintf( __( 'No %s found', 'theme-joker-core' ), $plural ),
            'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'theme-joker-core' ), $plural ),
            'parent_item_colon'  => sprintf( __( 'Parent %s:', 'theme-joker-core' ), $singular )
        );

        /* Post Type args Array */
        $args = array(
                    'public'        =>  true,
                    'labels'        =>  $labels,
                    'show_ui'       =>  true,
                    'query_var'     =>  true,
                    'has_archive'   =>  $post_type,
                    'rewrite'       =>  array( 'slug' => $slug ),
                    'supports'      =>  array( 'title'),
                    'publicly_queryable' => true,
                    'capability_type' => 'post',
                );

        // Merge user submitted options with defaults.
        $args = array_replace_recursive( $args, $this->args );

        // Set the object options as full options passed.
        $this->args = $args;

        // Check that the post type doesn't already exist.
        if ( ! post_type_exists( $this->post_type ) ) {
            // Register the post type.
            register_post_type( $this->post_type, $args );
        }

        if(in_array('thumbnail', (array)$this->args['supports'])):
            //Change Feature Image title
            $this->xtrimit_add_action( 'add_meta_boxes', array($this, 'xtrimit_featured_image_title') );
            //Change Feature Image Link Test
            $this->xtrimit_add_filter( 'admin_post_thumbnail_html', array($this, 'xtrimit_featured_image_content') );
            $this->xtrimit_add_filter( 'media_view_strings', array($this, 'xtrimit_featured_image_media_strings'), 10, 2 );
        endif;
    }


    /**
     * Change Custom Post Title Placeholder
     *
     * @param $title
    */

    function xtrimit_post_title( $title ){
        $screen = get_current_screen();
        if  ( $this->post_type == $screen->post_type ):
          $title = $this->singular . ' Name';
        endif;

        return $title;
    }

    /**
     * Change Feature Image title
     *
     * @param $post_type
     */
    function xtrimit_featured_image_title($post_type) {
        if ( $post_type === $this->post_type ) {
            //remove original featured image metabox
            remove_meta_box( 'postimagediv', $this->post_type, 'side' );
            //add our customized metabox
            add_meta_box( 'postimagediv', $this->singular . ' Image', 'post_thumbnail_meta_box', $this->post_type, 'side', 'low' );
        }
    }

    /**
     * Change Feature Image Link Test 
     *
     * @param string $content
     *
     * @return string
     */
    function xtrimit_featured_image_content($content) {
        $content = str_replace( __( 'Set featured image', 'theme-joker-core' ), 'Add Image', $content );
        $content = str_replace( __( 'Remove featured image', 'theme-joker-core' ), 'Remove Image', $content );
        return $content;
    }


    /**
     * Change the strings for the media manager modal
     *
     * @param $strings
     * @param $post
     *
     * @return mixed
     */
    function xtrimit_featured_image_media_strings($strings, $post) {

        // checks if post object is empty or not
        if ( ! empty($post) ) {
            if ( $post->post_type === $this->post_type ) {
                $strings['setFeaturedImage']      = "Set {$this->singular}";
                $strings['setFeaturedImageTitle'] = "{$this->singular} Image";
            }
        }
        return $strings;
    }
}


/**
 * Custom Taxonomy Class
 * Used to help create custom Taxonomy for Wordpress.
 * @author  Saiful Islam Ananda
 * @link    http://saifulananda.me/
 * @version 1.0.0
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class TJoker_CTT extends TJoker_CPT{


    /**
    * Taxonomy name.
    *
    * @var string $taxnomoy_name Holds the name of the Taxnomoy.
    */
    private $taxnomoy_names;

    /**
    * Exisiting taxonomies to be registered after the post has been registered
    *
    * @var array $exisiting_taxonomies holds exisiting taxonomies
    */
    public $exisiting_taxonomies;


    
    function __construct($taxnomoy_names, $post_type,  $singular = "", $plural = "",  $args = array()){
        
        $this->post_type    =   strtolower(str_replace(' ', '-', $post_type));
        if(!empty($singular)):
            $this->singular     =   $singular;
        else:
            $this->singular     =   $this->xtrimit_get_singular($post_type);
        endif;
        if(!empty($plural)):
            $this->plural     =   $plural;
        else:
            $this->plural     =   $this->xtrimit_get_plural($post_type);
        endif;
        
        $this->slug         =   $this->xtrimit_get_slug($this->post_type);
        
        $this->args         = (array)$args;
        
        $this->taxnomoy_names   =   $taxnomoy_names;


        // print_r($this->post_type);
        // exit(0);
        
        
        // Register the taxnomoy type.
        $this->xtrimit_init(array(&$this, 'xtrimit_register_taxonomies'));
        // Register exisiting taxonomies.
        $this->xtrimit_init( array( &$this, 'xtrimit_register_exisiting_taxonomies' ) );
        
    }

    /**
    * Register taxonomies
    *
    * Cycles through taxonomies added with the class and registers them.
    */
    public function xtrimit_register_taxonomies() {

        // print_r($this->taxnomoy_names);
        // exit(0);
        // 
        $taxonomy_name =$this->taxnomoy_names;

        if (is_array($this->taxnomoy_names)):

            // Foreach taxonomy registered with the post type.
            foreach ($this->taxnomoy_names as $taxonomy_name):
                $taxonomy_name = $this->xtrimit_get_slug($taxonomy_name);
                // Register the taxonomy if it doesn't exist.
                if (!taxonomy_exists($taxonomy_name)):
                    //$taxonomy_name = $this->xtrimit_get_slug($taxonomy_name);
                    // Register the taxonomy with Wordpress
                    $this->xtrimit_register_taxonomy( $taxonomy_name, $this->post_type, $this->args );

                else:

                    // If taxonomy exists, register it later with register_exisiting_taxonomies
                    $this->exisiting_taxonomies[] = $taxonomy_name;
                endif;
            endforeach;
        else:
            $taxonomy_name = $this->xtrimit_get_slug($taxonomy_name);
            $this->xtrimit_register_taxonomy( $taxonomy_name, $this->post_type, $this->args );
        endif;
    }


    /**
    * Register taxonomy
    *
    * @see http://codex.wordpress.org/Function_Reference/register_taxonomy
    *
    * @param string $taxonomy_name The slug for the taxonomy.
    * @param array  $args Taxonomy args.
    */

    public function xtrimit_register_taxonomy($taxonomy_name, $post_type, $args = array()) {

        // Post type defaults to $this post type if unspecified.
        $post_type = $post_type;

        $singular = $this->singular;
        $plural = $this->plural;
        $slug = $this->slug;
        // Create user friendly names.
        // $taxonomy_name  = $this->taxnomoy_name;
        // $singular       = $this->singular;
        // $plural         = $this->plural;
        // $slug           = $this->slug;

        // Default labels.
        $labels = array(
            'name'                       => sprintf( __( '%s', 'theme-joker-core' ), $singular ),
            'singular_name'              => sprintf( __( '%s', 'theme-joker-core' ), $singular ),
            'menu_name'                  => sprintf( __( '%s', 'theme-joker-core' ), $singular ),
            'all_items'                  => sprintf( __( 'All %s', 'theme-joker-core' ), $singular ),
            'edit_item'                  => sprintf( __( 'Edit %s', 'theme-joker-core' ), $singular ),
            'view_item'                  => sprintf( __( 'View %s', 'theme-joker-core' ), $singular ),
            'update_item'                => sprintf( __( 'Update %s', 'theme-joker-core' ), $singular ),
            'add_new_item'               => sprintf( __( 'Add New %s', 'theme-joker-core' ), $singular ),
            'new_item_name'              => sprintf( __( 'New %s Name', 'theme-joker-core' ), $singular ),
            'parent_item'                => sprintf( __( 'Parent %s', 'theme-joker-core' ), $singular ),
            'parent_item_colon'          => sprintf( __( 'Parent %s:', 'theme-joker-core' ), $singular ),
            'search_items'               => sprintf( __( 'Search %s', 'theme-joker-core' ), $singular ),
            'popular_items'              => sprintf( __( 'Popular %s', 'theme-joker-core' ), $singular ),
            'separate_items_with_commas' => sprintf( __( 'Seperate %s with commas', 'theme-joker-core' ), $singular ),
            'add_or_remove_items'        => sprintf( __( 'Add or remove %s', 'theme-joker-core' ), $singular ),
            'choose_from_most_used'      => sprintf( __( 'Choose from most used %s', 'theme-joker-core' ), $singular ),
            'not_found'                  => sprintf( __( 'No %s found', 'theme-joker-core' ), $singular ),
        );

        // Default options.

        $args = array(
        'labels'                => $labels,
        //'description'           => '',
        //'public'                => true,
        'hierarchical'          => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_nav_menus'     => true,
        //'show_tagcloud'         => null,
        'show_in_quick_edit'    => true,
        'show_admin_column'     => true,
        //'meta_box_cb'           => null,
        //'capabilities'          => array(),
        'rewrite'               => array($slug = $this->slug),
        'query_var'             => true,
        'update_count_callback' => '',
        //'_builtin'              => false,
    );

        // Merge default options with user submitted options.
        $args = array_replace_recursive( $args, $this->args );

        // // Add the taxonomy to the object array, this is used to add columns and filters to admin panel.
        // $this->taxonomies[] = $taxonomy_name;

        // // Create array used when registering taxonomies.
        // $this->taxonomy_settings[ $taxonomy_name ] = $options;
        
        register_taxonomy($taxonomy_name, $post_type, $args);

    }

    /**
     * Register Exisiting Taxonomies
     *
     * Cycles through exisiting taxonomies and registers them after the post type has been registered
     */
    function xtrimit_register_exisiting_taxonomies() {

        if( is_array( $this->exisiting_taxonomies ) ) {
            foreach( $this->exisiting_taxonomies as $taxonomy_name ) {
                register_taxonomy_for_object_type( $taxonomy_name, $this->post_type );
            }
        }
    }
}
