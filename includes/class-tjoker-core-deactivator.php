<?php
/**
 * Fired during plugin deactivation
 *
 * @link       http://themejoker.com/
 * @since      1.0.0
 *
 * @package    TJoker_Core
 * @subpackage TJoker_Core/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    TJoker_Core
 * @subpackage TJoker_Core/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */
class TJoker_Core_Deactivator
{

    /**
     * 
     * @since    1.0.0
     */
    public static function deactivate()
    {
       
            
    }
}
