<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://themejoker.com/
 * @since      1.0.0
 *
 * @package    TJoker_Core
 * @subpackage TJoker_Core/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    TJoker_Core
 * @subpackage TJoker_Core/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */
class TJoker_Core
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      TJoker_Core_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.1.0
     */
    public function __construct()
    {

        $this->plugin_name = 'theme-joker-core';
        $this->version = '1.0.0';

        $this->tj_load_dependencies();
        $this->tj_set_locale();
        $this->tj_admin_hooks_define();
        $this->tj_public_hooks_define();

        $this->tj_load_cpt();
        $this->tj_load_shortcode();

    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - TJoker_Core_Loader. Orchestrates the hooks of the plugin.
     * - TJoker_Core_i18n. Defines internationalization functionality.
     * - TJoker_Core_Admin. Defines all hooks for the admin area.
     * - TJoker_Core_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function tj_load_dependencies()
    {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-tjoker-core-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-tjoker-core-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-tjoker-core-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-tjoker-core-public.php';

         /**
         * The class responsible for custom post type functionlity
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-tjoker-cpt.php';

        /**
         * The file responsible for Script Generate.
         */

        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-tjoker-script-generator.php';

         /**
         * The class responsible for all metabox functionality
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/metaboxes/meta-box.php';

         /**
         * The class responsible for all Theme Options functionality
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/ReduxCore/framework.php';

         /**
         * The file responsible for all helper fnction.
         */

        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/helper/init.php';

         /**
         * The file responsible for all THeme Options fnctionality.
         */
        if ( class_exists( 'ReduxFramework' ) && file_exists(plugin_dir_path(dirname(__FILE__)) . 'includes/ReduxCore/config.php')):
            require_once plugin_dir_path(dirname(__FILE__)) . 'includes/ReduxCore/config.php';
        endif;
        $this->loader = new TJoker_Core_Loader();
    } 

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the TJoker_Core_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function tj_set_locale()
    {

        $plugin_i18n = new TJoker_Core_i18n();
        $plugin_i18n->set_domain($this->tj_get_plugin_name());

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'tj_load_plugin_textdomain');
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function tj_admin_hooks_define()
    {

        $plugin_admin = new TJoker_Core_Admin($this->tj_get_plugin_name(), $this->tj_get_version());
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'tj_enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'tj_enqueue_scripts');
        $this->loader->add_filter('get_search_form', $plugin_admin, 'tj_search_form',100);
        //$this->loader->add_action( 'widgets_init', $plugin_admin, 'tj_recent_posts_widget');
        $this->loader->add_filter('term_links-post_tag', $plugin_admin, 'tj_limit_post_tags');
        $this->loader->add_filter('term_links-category', $plugin_admin, 'tj_limit_post_category');

        //To keep the count accurate, lets get rid of prefetching
        remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
        // Track Post Views For Popular Post
        $this->loader->add_action('wp_head', $plugin_admin, 'tj_track_post_views');

    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */ 
    private function tj_public_hooks_define(){

        $plugin_public = new TJoker_Core_Public($this->tj_get_plugin_name(), $this->tj_get_version());

        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'tj_enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'tj_enqueue_scripts');
    }


    private function tj_load_shortcode()
    {

        /**
         * Load Shortcode INit file
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/shortcode/init.php';

    }

    private function tj_load_cpt()
    {

        /**
         * Load Custom Post Type INit file
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/posttype/init.php';

    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function tj_get_plugin_name()
    {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    TJoker_Core_Loader    Orchestrates the hooks of the plugin.
     */
    public function tj_get_loader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function tj_get_version()
    {
        return $this->version;
    }
}

