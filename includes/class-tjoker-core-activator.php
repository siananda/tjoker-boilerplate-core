<?php
/**
 * Fired during plugin activation
 *
 * @link       http://themejoker.com/
 * @since      1.0.0
 *
 * @package    TJoker_Core
 * @subpackage TJoker_Core/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    TJoker_Core
 * @subpackage TJoker_Core/includes
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */
class TJoker_Core_Activator
{

    /**
     *
     * @since    1.0.0
     */
    public static function activate(){


    }
}
