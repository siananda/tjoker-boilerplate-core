<?php
/**
 * Fired when the plugin is uninstalled.
 *
 *
 * @link       http://themejoker.com/
 * @since      1.0.0
 *
 * @package    TJoker_Core
 */
// If uninstall not called from WordPress, then exit.
if (!defined('WP_UNINSTALL_PLUGIN')) {
    exit;
}




