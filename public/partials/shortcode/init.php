<?php  

/**
 * Theme Joker Shortcodes setup.
 */

	// File Security Check
	if(!defined('ABSPATH')):
		exit;
	endif;

	$tj_shortcode = array(
				//'tj_news_style1',
		);

	$tj_files 	= array_map(function($e){return str_replace('tj_','',$e);}, $tj_shortcode);

	foreach ( $tj_files as $shortcode_file ):
		require_once plugin_dir_path(dirname(__FILE__)) . 'shortcode/' . $shortcode_file . '.php';
	endforeach;

	foreach ($tj_shortcode as $shortcode):
		if(function_exists("{$shortcode}")):
			add_shortcode("{$shortcode}", "{$shortcode}");
		endif;
	endforeach;
?>