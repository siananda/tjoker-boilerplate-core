<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @package    TJoker_Core
 * @subpackage TJoker_Core/admin
 * @author     Saiful Islam <anandacsebd@gmail.com>
 */
class TJoker_Core_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version){

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function tj_enqueue_styles(){

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/tjoker-core-admin.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function tj_enqueue_scripts(){

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/tjoker-core-admin.js', array('jquery'), $this->version, false);
    }

    public function tj_search_form( $form ) {
        $form = '<div class="input-group">
        <form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
            <input type="text" class="form-control" placeholder="Search" value="' . get_search_query() . '" name="s" id="s" />
            <span class="input-group-btn">
                <button type="button"><i class="fa fa-search"></i></button>
            </span>
        </form>
        </div>';
        return $form;
    }  
    
    public function tj_limit_post_tags($terms) {
        //print_r($terms);
        return array_slice($terms,0,3,true);
    }
    public function tj_limit_post_category($terms) {
        //print_r($terms);
        return array_slice($terms,0,2,true);
    }

    public function tj_track_post_views ($post_id) {
        if ( !is_single() ) return;
        if ( empty ( $post_id) ) {
            global $post;
            $post_id = $post->ID;    
        }
        $this->tj_set_post_views($post_id);
    }

    public function tj_set_post_views($postID) {
        $count_key = 'tj_post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if($count==''){
            $count = 0;
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
        }else{
            $count++;
            update_post_meta($postID, $count_key, $count);
        }
    }
}
