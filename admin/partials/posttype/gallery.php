<?php  

	// File Security Check
  	if(!defined('ABSPATH')):
    	exit;
  	endif;
  	$args = array(
                'supports'      =>  array('thumbnail'),
                'menu_icon'           => 'dashicons-format-gallery',
            );
  	
	$gallery = new TJoker_CPT('tjgallery', 'Gallery', 'Galleries',$args);


	/*-----------------------------------------------------------------------------------*/
	/*	Gallery Metaboxes
	/*-----------------------------------------------------------------------------------*/  

	$fields = array(
		array(
			'label'	=> 'Gallery Imge Serial',
			'desc'	=> 'Input your Gallery Image Serial Number. it\'s help you serialize your Gallery frontend section. if it\' 0 or nagetive value not show in frontend' ,
			'id'	=> "{$prefix}gallery_serial",
			'type'	=> 'number',
		),
		array(
			'label'	=> 'Gallery Title',
			'desc'	=> 'Input Gallery Title.',
			'id'	=> "{$prefix}gallery_name",
			'type'	=> 'text',
		),

	);

	$test_info = new custom_add_meta_box( 'tj_gallery_info', 'Gallery Information', $fields, 'tjgallery', true );


	add_filter( 'manage_edit-tjgallery_columns', 'tj_gallery_columns_title');
	function tj_gallery_columns_title( $columns ) {  
	    
		unset($columns[ 'date' ]);
		unset($columns[ 'title' ]);
		$columns[ 'name' ] = 'Title';
		$columns[ 'image' ] = 'Imge';
		$columns[ 'serial' ] = 'Serial';
		$columns[ 'xtbutton' ] = 'Button';
	    return $columns;  
	} 

	function tj_gallery_columns_data($column_name, $post_ID) {
	    
		switch ($column_name):
			case 'image':
				the_post_thumbnail(array(120, 120));
				break;
			case 'name':
				echo get_post_meta( $post_ID, 'tj_gallery_name', true);
				break;
				break;
			case 'serial':
				echo get_post_meta( $post_ID, 'tj_gallery_serial', true);
				break;
			case 'xtbutton':
				$xtrimIT = '';
				$admin_url = admin_url();
				$xtrimIT .= '<div class="xtrimIT-action">';
					$xtrimIT .= '<span class="edit xtrimIT-edit">';
						$xtrimIT .= '<a class="button" href="' . $admin_url . 'post.php?post=' . $post_ID . '&amp;action=edit" title="Edit this item">Edit</a>';
					$xtrimIT .= '</span>';
				$xtrimIT .= '</div>';
				echo "$xtrimIT";
				break;
			default:
				# code...
				break;
		endswitch;
	}

	add_action('manage_tjgallery_posts_custom_column', 'tj_gallery_columns_data', 10, 2);

