<?php  

	// File Security Check
  	if(!defined('ABSPATH')):
    	exit;
  	endif;
  	
	$args = array(
                'supports'      =>  array('thumbnail'),
                'menu_icon'           => 'dashicons-heart',
            );


	$testimonial = new TJoker_CPT('testimonial','','', $args);


	$fields = array(

		array(
			'label'	=> 'Client Name',
			'desc'	=> 'Input Project Live Link.',
			'id'	=> "{$prefix}client_name",
			'type'	=> 'text',
		),
		array(
			'label'	=> 'Client Designation',
			'desc'	=> 'Write Reviewer Designation.',
			'id'	=> "{$prefix}client_designation",
			'type'	=> 'text',
		),
		array( // Textarea
			'label'	=> 'Client Review',
			'desc'	=> 'Write client review into the textarea.',
			'id'	=> "{$prefix}client_review",
			'type'	=> 'textarea',
		),
		array(
			'label'	=> 'Client Signature',
			'desc'	=> 'Upload Client Signature Image..',
			'id'	=> "{$prefix}client_signature",
			'type'	=> 'image'
		),
		array(
			'label'	=> 'Testimonial Serial',
			'desc'	=> 'Input your Testimonial Serial Number. it\'s help you serialize your Testimonial frontend section. if it\' 0 or nagetive value not show in frontend' ,
			'id'	=> "{$prefix}testimonial_serial",
			'type'	=> 'number',
		),
	);

	$test_info = new custom_add_meta_box( 'tj_test_info', 'Testimonial Information', $fields, 'testimonial', true );


	add_filter( 'manage_edit-testimonial_columns', 'tj_testimonial_columns_title');
	function tj_testimonial_columns_title( $columns ) {  
	    
		unset($columns[ 'date' ]);
		unset($columns[ 'title' ]);
		$columns[ 'reviewer-name' ] = 'Client Name';
		$columns[ 'reviewer-image' ] = 'Client Image';
		$columns[ 'reviewer-des' ] = 'Review';
		$columns[ 'serial' ] = 'Serial';
		$columns[ 'xtbutton' ] = 'Button';
	    return $columns;  
	} 



	function tj_testimonial_columns_data($column_name, $post_ID) {
	    
		switch ($column_name):
			
			case 'reviewer-name':
				echo get_post_meta( $post_ID, 'tj_client_name', true);
				break;
			case 'reviewer-image':
				the_post_thumbnail(array(45, 45));
				break;

			case 'reviewer-des':
				echo wp_trim_words(get_post_meta( $post_ID, 'tj_client_review', true),10,'');
				break;
			case 'serial':
				echo get_post_meta( $post_ID, 'tj_testimonial_serial', true);
				break;
			case 'xtbutton':
				$xtrimIT = '';
				$admin_url = admin_url();
				$xtrimIT .= '<div class="xtrimIT-action">';
					$xtrimIT .= '<span class="edit xtrimIT-edit">';
						$xtrimIT .= '<a class="button" href="' . $admin_url . 'post.php?post=' . $post_ID . '&amp;action=edit" title="Edit this item">Edit</a>';
					$xtrimIT .= '</span>';
				$xtrimIT .= '</div>';
				echo "$xtrimIT";
				break;
			default:
				# code...
				break;
		endswitch;
	}

	add_action('manage_testimonial_posts_custom_column', 'tj_testimonial_columns_data', 10, 2);



?>