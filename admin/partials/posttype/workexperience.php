<?php  

	// File Security Check
  	if(!defined('ABSPATH')):
    	exit;
  	endif;
  	
	$args = array(
                'supports'      =>  array('title'),
                'menu_icon'           => 'dashicons-text',
            );


	$workexp = new TJoker_CPT('workexperience', 'Work Experience', 'Work Experiences',$args);


	$fields = array(
		array(
			'label'	=> 'Experience Year',
			'desc'	=> 'Input your Experience Year.',
			'id'	=> "{$prefix}workexp_year",
			'type'	=> 'text',
		),
		array(
			'label'	=> 'Company Name',
			'desc'	=> 'Input Company Name.',
			'id'	=> "{$prefix}workexp_company",
			'type'	=> 'text',
		),
		array(
			'label'	=> 'Responsiblity',
			'desc'	=> 'Write your working Responsiblity for this company.',
			'id'	=> "{$prefix}workexp_respons",
			'type'	=> 'textarea',
		),
		array(
			'label'	=> 'Work Experience Serial',
			'desc'	=> 'Input your Work Experience Serial Number. it\'s help you serialize your Work Experience frontend section.' ,
			'id'	=> "{$prefix}workexp_serial",
			'type'	=> 'number',
		),
	);

	$test_info = new custom_add_meta_box( 'xt_price_pack_info', 'Experience Information', $fields, 'workexperience', true );


	add_filter( 'manage_edit-workexperience_columns', 'tj_work_experience_columns_title');
	function tj_work_experience_columns_title( $columns ) {  
	    
		unset($columns[ 'date' ]);
		//unset($columns[ 'title' ]);
		$columns[ 'title' ] = 'Position';
		$columns[ 'workexp_company' ] = 'Conpany Name';
		$columns[ 'workexp_year' ] = 'Working Year';
		$columns[ 'workexp_serial' ] = 'Serial';
	    return $columns;  
	} 


 
	function tj_work_experience_columns_data($column_name, $post_ID) {
	    
		switch ($column_name):
			
			case 'workexp_company':
				echo get_post_meta( $post_ID, 'tj_workexp_company', true);
				break;
			case 'workexp_year':
				echo get_post_meta( $post_ID, 'tj_workexp_year', true);
				break;
			case 'workexp_serial':
				echo get_post_meta( $post_ID, 'tj_workexp_serial', true);
				break;
			default:
				# code...
				break;
		endswitch;
	}

	add_action('manage_workexperience_posts_custom_column', 'tj_work_experience_columns_data', 10, 2);



?>