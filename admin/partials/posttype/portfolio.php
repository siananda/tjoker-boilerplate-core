<?php 

	// File Security Check
  	if(!defined('ABSPATH')):
    	exit;
  	endif;
	
	$args = array(
                'supports'      =>  array('title' ,'thumbnail'),
                'menu_icon'           => 'dashicons-portfolio',
            );

	$portfolios = new TJoker_CPT('portfolio','Portfolio','Portfolios',$args);

	$tj_pfilter = new TJoker_CTT('tjfcat', 'portfolio', 'Filter Category', 'Filter Category');

	$tj_pcat = new TJoker_CTT('tjpcat', 'portfolio', 'Portfolio Category', 'Portfolio Category');


	$fields = array(
		array(
			'label'	=> 'Client Name',
			'desc'	=> 'Input Portfolio Client Name.',
			'id'	=> "{$prefix}portfolio_client",
			'type'	=> 'text',
		),
		array(
			'label'	=> 'Project Task',
			'desc'	=> 'Input project Task.',
			'id'	=> "{$prefix}portfolio_task",
			'type'	=> 'text',
		),
		array(
			'label'	=> 'Ending Date', 
			'desc'	=> 'Input Portfolio Ending Date', 
			'id'	=> "{$prefix}portfolio_date",
			'type'	=> 'date',
		),
		array( // Textarea
			'label'	=> 'Project Description',
			'desc'	=> 'Write Project Description',
			'id'	=> "{$prefix}portfolio_des",
			'type'	=> 'textarea',
			'sanitizer' => 'tj_wp_kses',
		),
		array(
			'label'	=> 'Live Link',
			'desc'	=> 'Input Portfolio Live Link.',
			'id'	=> "{$prefix}portfolio_link",
			'type'	=> 'url',
		),
	);

	$portfolio_info = new custom_add_meta_box( 'tj_pro_info', 'Portfolio Information', $fields, 'portfolio', true );


// Add term page
function xtrimIT_xtfiltercat_add_new_meta_field() {
	$XThtml = '';
	$XThtml .= '<div class="form-field">';
		$XThtml .= '<label for="term_meta[cat_icon_term_meta]">'.sprintf(__('Category Icon','tjpinch')).'</label>';
		$XThtml .= '<input type="text" name="term_meta[cat_icon_term_meta]" id="term_meta[cat_icon_term_meta]" value="">';
		$XThtml .= '<p class="description">'.sprintf(__('Input Your Category Icon Class','tjpinch')).'</p>';
	$XThtml .= '</div>';
	echo "{$XThtml}";
}
add_action( 'xtfiltercat_add_form_fields', 'xtrimIT_xtfiltercat_add_new_meta_field', 10, 2 );

// Edit term page
function xtrimIT_xtfiltercat_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "taxonomy_$t_id" );

	$xtvalue = esc_attr( $term_meta['cat_icon_term_meta'] ) ? esc_attr( $term_meta['cat_icon_term_meta'] ) : '';

	$XThtml = '';
	$XThtml .= '<tr class="form-field">';
		$XThtml .= '<th scope="row" valign="top"><label for="term_meta[cat_icon_term_meta]">'.sprintf(__('Category Icon','tjpinch')).'</label></th>';
		$XThtml .= '<td>';
			$XThtml .= '<input type="text" name="term_meta[cat_icon_term_meta]" id="term_meta[cat_icon_term_meta]" value="'.$xtvalue.'">';
			$XThtml .= '<p class="description">'.sprintf(__('Input Your Category Icon Class','tjpinch')).'</p>';
		$XThtml .= '</td>';
	$XThtml .= '</tr>';
	echo "{$XThtml}";
}
add_action( 'xtfiltercat_edit_form_fields', 'xtrimIT_xtfiltercat_edit_meta_field', 10, 2 );

// Save extra taxonomy fields callback function.
function save_taxonomy_custom_meta( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "taxonomy_$t_id", $term_meta );
	}
}  
add_action( 'edited_xtfiltercat', 'save_taxonomy_custom_meta', 10, 2 );  
add_action( 'create_xtfiltercat', 'save_taxonomy_custom_meta', 10, 2 );